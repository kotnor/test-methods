package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.exceptions.*;

/**
 * Created by Александр on 09.10.2016.
 */
public class PlanTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    @Test
    public void testCreateNewPlan() {
        new Plan("Name", "SomeDetails", 30, 20, 10);
    }
    @Test
    public void testCreateNewPlanWithNullName() {
        expectedEx.expect(PlanNameException.class);
        expectedEx.expectMessage("Plan Name is null");
        new Plan(null, "SomeDetails", 30, 20, 10);
    }
    @Test
    public void testCreateNewPlanWithShortName() {
        expectedEx.expect(PlanNameException.class);
        expectedEx.expectMessage("Service Plan Name is too short");
        new Plan("n", "SomeDetails", 30, 20, 10);
    }
    @Test
    public void testCreateNewPlanWithShortAvailableName() {
        new Plan("Na", "SomeDetails", 30, 20, 10);
    }
    @Test
    public void testCreateNewPlanWithLongAvailableName() {
        String name128 = "HelloathisaisamyHelloathisaisamyHelloathisaisamyHelloathisaisamyHelloathisaisamyHelloathisaisamyHelloathisaisamyHelloathisaisamy";
        new Plan(name128, "SomeDetails", 30, 20, 10);
    }
    @Test
    public void testCreateNewPlanWithLongName() {
        String name129 = "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my1";
        expectedEx.expect(PlanNameException.class);
        expectedEx.expectMessage("Plan Name is too long");
        new Plan(name129, "SomeDetails", 30, 20, 10);
    }
    @Test
    public void testCreateNewPlanWithNameWithWrongSymbol() {
        expectedEx.expect(PlanNameException.class);
        expectedEx.expectMessage("Plan Name contains wrong symbol");
        new Plan("Na@me", "SomeDetails", 30, 20, 10);
    }

    @Test
    public void testCreateNewPlanWithNullDetails() {
        expectedEx.expect(PlanDetailsException.class);
        expectedEx.expectMessage("Plan details is null");
        new Plan("Name", null, 30, 20, 10);
    }
    @Test
    public void testCreateNewPlanWithShortDetails() {
        expectedEx.expect(PlanDetailsException.class);
        expectedEx.expectMessage("Plan details length is too short");
        new Plan("Name", "", 30, 20, 10);
    }
    @Test
    public void testCreateNewPlanWithShortAvailableDetails() {
        new Plan("Name", "S", 30, 20, 10);
    }
    @Test
    public void testCreateNewPlanWithLongAvailableDetails() {
        String details1024 = "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my" +
                "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my" +
                "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my" +
                "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my" +
                "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my" +
                "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my" +
                "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my" +
                "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my";
        new Plan("Name", details1024, 30, 20, 10);
    }
    @Test
    public void testCreateNewPlanWithLongDetails() {
        String details1024 = "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my" +
                "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my" +
                "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my" +
                "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my" +
                "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my" +
                "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my" +
                "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my" +
                "Hello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is myHello,this is my1";

        expectedEx.expect(PlanDetailsException.class);
        expectedEx.expectMessage("Plan details length is too long");
        new Plan("Name", details1024, 30, 20, 10);
    }

    @Test
    public void testCreateNewPlanWithLittleMaxSeats() {
        expectedEx.expect(PlanMaxSeatsException.class);
        expectedEx.expectMessage("Plan max seats value is too small");
        new Plan("Name", "SomeDetails", 0, 20, 10);
    }
    @Test
    public void testCreateNewPlanWithLittleAvailableMaxSeats() {
        new Plan("Name", "SomeDetails", 1, 1, 10);
    }
    @Test
    public void testCreateNewPlanWithBigAvailableMaxSeats() {
        new Plan("Name", "SomeDetails", 999999, 20, 10);
    }
    @Test
    public void testCreateNewPlanWithBigMaxSeats() {
        expectedEx.expect(PlanMaxSeatsException.class);
        expectedEx.expectMessage("Plan max seats value is too big");
        new Plan("Name", "SomeDetails", 1000000, 20, 10);
    }


    @Test
    public void testCreateNewPlanWithLittleMinSeats() {
        expectedEx.expect(PlanMinSeatsException.class);
        expectedEx.expectMessage("Plan min seats value is too small");
        new Plan("Name", "SomeDetails", 30, 0, 10);
    }
    @Test
    public void testCreateNewPlanWithLittleAvailableMinSeats() {
        new Plan("Name", "SomeDetails", 30, 1, 10);
    }
    @Test
    public void testCreateNewPlanWithBigAvailableMinSeats() {
        new Plan("Name", "SomeDetails", 999999, 999999, 10);
    }
    @Test
    public void testCreateNewPlanWithBigMinSeats() {
        expectedEx.expect(PlanMinSeatsException.class);
        expectedEx.expectMessage("Service Plan min seats value is too big");
        new Plan("Name", "SomeDetails", 999999, 1000000, 10);
    }
    @Test
    public void testCreateNewPlanWithMinSeatsBiggerMaxSeats() {
        expectedEx.expect(PlanMinSeatsException.class);
        expectedEx.expectMessage("Service Plan min seats value bigger short than max seats");
        new Plan("Name", "SomeDetails", 20, 30, 10);
    }
    @Test
    public void testCreateNewPlanWithLittleFeePerUnit() {
        expectedEx.expect(PlanFeePerUnitException.class);
        expectedEx.expectMessage("Service Plan feePerUnit value is too small");
        new Plan("Name", "SomeDetails", 30, 20, -1);
    }
    @Test
    public void testCreateNewPlanWithLittleAvailableFeePerUnit() {
        new Plan("Name", "SomeDetails", 30, 20, 0);
    }
    @Test
    public void testCreateNewPlanWithBigAvailableFeePerUnit() {
        new Plan("Name", "SomeDetails", 30, 20, 999999);
    }
    @Test
    public void testCreateNewPlanWithBigFeePerUnit() {
        expectedEx.expect(PlanFeePerUnitException.class);
        expectedEx.expectMessage("Service Plan feePerUnit value is too big");
        new Plan("Name", "SomeDetails", 30, 20, 1000000);
    }
}
