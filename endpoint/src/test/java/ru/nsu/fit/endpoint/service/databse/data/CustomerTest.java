package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.exceptions.*;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CustomerTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewCustomer() {
        new Customer("John", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }

    @Test
    public void testCreateNewCustomerWithNullFirstName() {
        expectedEx.expect(CustomerFirstNameException.class);
        expectedEx.expectMessage("First name is null");
        new Customer(null, "Wick", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithShortFirstName() {
        expectedEx.expect(CustomerFirstNameException.class);
        expectedEx.expectMessage("First name is too short");
        new Customer("J", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithShortAvailableFirstName() {
        new Customer("Jo", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithLongAvailableFirstName() {
        new Customer("Johnjohnjohn", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithLongFirstName() {
        expectedEx.expect(CustomerFirstNameException.class);
        expectedEx.expectMessage("First name is too long");
        new Customer("Johnjohnjohnj", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithFirstNameWithSpace() {
        expectedEx.expect(CustomerFirstNameException.class);
        expectedEx.expectMessage("First name contains space");
        new Customer("John john", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithFirstNameWithFirstCapitalLetter() {
        expectedEx.expect(CustomerFirstNameException.class);
        expectedEx.expectMessage("First name should start capital letter");
        new Customer("john", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithFirstNameWithMoreFirstCapitalLetter() {
        expectedEx.expect(CustomerFirstNameException.class);
        expectedEx.expectMessage("There is a capital letter after 1 symbol");
        new Customer("JOhn", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithFirstNameWithDigit() {
        expectedEx.expect(CustomerFirstNameException.class);
        expectedEx.expectMessage("First name contains digit");
        new Customer("J1hn", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithFirstNameWithWrongSymbol() {
        expectedEx.expect(CustomerFirstNameException.class);
        expectedEx.expectMessage("First name contains wrong symbol");
        new Customer("J&hn", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }

    @Test
    public void testCreateNewCustomerWithNullLastName() {
        expectedEx.expect(CustomerLastNameException.class);
        expectedEx.expectMessage("Last name is null");
        new Customer("John", null, "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithShortLastName() {
        expectedEx.expect(CustomerLastNameException.class);
        expectedEx.expectMessage("Last name is too short");
        new Customer("John", "W", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithShortAvailableLastName() {
        new Customer("John", "Wi", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithLongAvailableLastName() {
        new Customer("John", "Wickwickwick", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithLongLastName() {
        expectedEx.expect(CustomerLastNameException.class);
        expectedEx.expectMessage("Last name is too long");
        new Customer("John", "Wickwickwickw", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithLastNameWithSpace() {
        expectedEx.expect(CustomerLastNameException.class);
        expectedEx.expectMessage("Last name contains space");
        new Customer("John", "Wick wick", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithLastNameWithFirstCapitalLetter() {
        expectedEx.expect(CustomerLastNameException.class);
        expectedEx.expectMessage("Last name should start capital letter");
        new Customer("John", "wick", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithLastNameWithMoreFirstCapitalLetter() {
        expectedEx.expect(CustomerLastNameException.class);
        expectedEx.expectMessage("There is a capital letter after 1 symbol");
        new Customer("John", "WIck", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithLastNameWithDigit() {
        expectedEx.expect(CustomerLastNameException.class);
        expectedEx.expectMessage("Last name contains digit");
        new Customer("John", "W1ck", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithLastNameWithWrongSymbol() {
        expectedEx.expect(CustomerLastNameException.class);
        expectedEx.expectMessage("Last name contains wrong symbol");
        new Customer("John", "W&ck", "john_wick@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithEmailWithoutAt() {
        expectedEx.expect(CustomerEmailException.class);
        expectedEx.expectMessage("Login must be e-mail");
        new Customer("John", "Wick", "john_wickgmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithEmailWithMoreOneAt() {
        expectedEx.expect(CustomerEmailException.class);
        expectedEx.expectMessage("E-mail must contain only one symbol @");
        new Customer("John", "Wick", "john_wick@gm@ail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithEmailWithEmptyUserName() {
        expectedEx.expect(CustomerEmailException.class);
        expectedEx.expectMessage("Empty username in e-mail");
        new Customer("John", "Wick", "@gmail.com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithEmailWithoutDot() {
        expectedEx.expect(CustomerEmailException.class);
        expectedEx.expectMessage("Domain name doesnt contain dot symbol");
        new Customer("John", "Wick", "johnwick@gmailcom", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithEmailWithSeveralDot() {
        expectedEx.expect(CustomerEmailException.class);
        expectedEx.expectMessage("Domain name contains several dot symbol in sequence");
        new Customer("John", "Wick", "johnwick@gmail..com", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithEmailWithAtAndDot() {
        expectedEx.expect(CustomerEmailException.class);
        expectedEx.expectMessage("Incorrect domain name in login");
        new Customer("John", "Wick", "johnwick@.gmailcom", "strongpass", 0);
    }
    @Test
    public void testCreateNewCustomerWithEmailWithoutDomainTopLevel() {
        expectedEx.expect(CustomerEmailException.class);
        expectedEx.expectMessage("Empty top level domain in login");
        new Customer("John", "Wick", "johnwick@gmail.", "strongpass", 0);
    }

    @Test
    public void testCreateNewCustomerWithNullPass() {
        expectedEx.expect(CustomerPassException.class);
        expectedEx.expectMessage("Password is null");
        new Customer("John", "Wick", "john_wick@gmail.com", null, 0);
    }


    @Test
    public void testCreateNewCustomerWithShortPass() {
        expectedEx.expect(CustomerPassException.class);
        expectedEx.expectMessage("Password is too short");
        new Customer("John", "Wick", "john_wick@gmail.com", "stron", 0);
    }

    @Test
    public void testCreateNewCustomerWithAvailableShortPass() {
        new Customer("John", "Wick", "john_wick@gmail.com", "strong", 0);
    }

    @Test
    public void testCreateNewCustomerWithAvailableLongPass() {
        new Customer("John", "Wick", "john_wick@gmail.com", "strongstrong", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongPass() {
        expectedEx.expect(CustomerPassException.class);
        expectedEx.expectMessage("Password is too long");
        new Customer("John", "Wick", "john_wick@gmail.com", "123qwe123qwe1", 0);
    }

    @Test
    public void testCreateNewCustomerWithLoginPass() {
        expectedEx.expect(CustomerPassException.class);
        expectedEx.expectMessage("Password contains login");
        new Customer("John", "Wick", "john_wick@gmail.com", "john_wick", 0);
    }

    @Test
    public void testCreateNewCustomerWithFirstNamePass() {
        expectedEx.expect(CustomerPassException.class);
        expectedEx.expectMessage("Password contains first name");
        new Customer("John", "Wick", "john_wick@gmail.com", "John123", 0);
    }
    @Test
    public void testCreateNewCustomerWithLastNamePass() {
        expectedEx.expect(CustomerPassException.class);
        expectedEx.expectMessage("Password contains last name");
        new Customer("John", "Wick", "john_wick@gmail.com", "Wick123", 0);
    }
    @Test
    public void testCreateNewCustomerWithNegativeAccount() {
        expectedEx.expect(CustomerMoneyException.class);
        expectedEx.expectMessage("Account must be positive value");
        new Customer("John", "Wick", "john_wick@gmail.com", "strongpass", -1);
    }

    /*@Test
    public void testCreateNewCustomerWithEasyPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password is easy");
        new Customer("John", "Wick", "john_wick@gmail.com", "123qwe", 0);
    }*/
}
