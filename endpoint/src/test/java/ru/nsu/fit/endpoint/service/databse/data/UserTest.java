package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.service.database.exceptions.*;

/**
 * Created by Student_1 on 09.10.2016.
 */
public class UserTest {
        @Rule
        public ExpectedException expectedEx = ExpectedException.none();

        @Test
        public void testCreateNewUser() {
            new User("John", "Wick", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserWithNullFirstName() {
            expectedEx.expect(UserFirstNameException.class);
            expectedEx.expectMessage("First name is null");
            new User(null, "Wick", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserWithShortFirstName() {
            expectedEx.expect(UserFirstNameException.class);
            expectedEx.expectMessage("First name is too short");
            new User("A", "Wick", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserWithShortPermissibleFirstName() {
            new User("Ab", "Wick", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserWithLongPermissibleFirstName() {
            new User("Alicebobtomc", "Wick", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserWithLongFirstName() {
            expectedEx.expect(UserFirstNameException.class);
            expectedEx.expectMessage("First name is too long");
            new User("Alicebobtomca", "Wick", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserWithGapsInFirstName() {
            expectedEx.expect(UserFirstNameException.class);
            expectedEx.expectMessage("First name contains space");
            new User("Alice bob", "Wick", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserFirstNameStartWithCapitalLetter() {
            expectedEx.expect(UserFirstNameException.class);
            expectedEx.expectMessage("First name should start capital letter");
            new User("alice", "Wick", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserFirstNameContainsCapitalLetterAfterFirstSymbol() {
            expectedEx.expect(UserFirstNameException.class);
            expectedEx.expectMessage("There is a capital letter after 1 symbol");
            new User("ALice", "Wick", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserFirstNameContainsDigit() {
            expectedEx.expect(UserFirstNameException.class);
            expectedEx.expectMessage("First name contains digit");
            new User("A2ice", "Wick", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserFirstNameContainsWrongSymbol() {
            expectedEx.expect(UserFirstNameException.class);
            expectedEx.expectMessage("First name contains wrong symbol");
            new User("A@ice", "Wick", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserLastNameIsNull() {
            expectedEx.expect(UserLastNameException.class);
            expectedEx.expectMessage("Last name is null");
            new User("Alice", null, "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserLastNameIsTooShort() {
            expectedEx.expect(UserLastNameException.class);
            expectedEx.expectMessage("Last name is too short");
            new User("Alice", "B", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserLastNameIsPermissiblyShort() {
            new User("Alice", "Bo", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserLastNameIsPermissiblyLong() {
            new User("Alice", "Bobbbbbbbbbb", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserLastNameIsTooLong() {
            expectedEx.expect(UserLastNameException.class);
            expectedEx.expectMessage("Last name is too long");
            new User("Alice", "Bobbbbbbbbbbb", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserLastNameContainsGaps() {
            expectedEx.expect(UserLastNameException.class);
            expectedEx.expectMessage("Last name contains space");
            new User("Alice", "Bob bb", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserLastNameDoesNotStartsWithCapital() {
            expectedEx.expect(UserLastNameException.class);
            expectedEx.expectMessage("Last name should start capital letter");
            new User("Alice", "bob", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserLastNameContainsCapitalAfterFirstSymbol() {
            expectedEx.expect(UserLastNameException.class);
            expectedEx.expectMessage("There is a capital letter after 1 symbol");
            new User("Alice", "BBob", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserLastNameContainsDigit() {
            expectedEx.expect(UserLastNameException.class);
            expectedEx.expectMessage("Last name contains digit");
            new User("Alice", "B2ob", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserLastNameContainsWrongSymbols() {
            expectedEx.expect(UserLastNameException.class);
            expectedEx.expectMessage("Last name contains wrong symbol");
            new User("Alice", "B&ob", "john_wick@gmail.com", "strongpass");
        }

        @Test
        public void testCreateNewUserWithEmailWithoutAt() {
            expectedEx.expect(UserEmailException.class);
            expectedEx.expectMessage("Login must be e-mail");
            new User("John", "Wick", "john_wickgmail.com", "strongpass");
        }
        @Test
        public void testCreateNewUserWithEmailWithMoreOneAt() {
            expectedEx.expect(UserEmailException.class);
            expectedEx.expectMessage("E-mail must contain only one symbol @");
            new User("John", "Wick", "john_wick@gm@ail.com", "strongpass");
        }
        @Test
        public void testCreateNewUserWithEmailWithEmptyUserName() {
            expectedEx.expect(UserEmailException.class);
            expectedEx.expectMessage("Empty username in e-mail");
            new User("John", "Wick", "@gmail.com", "strongpass");
        }
        @Test
        public void testCreateNewUserWithEmailWithoutDot() {
            expectedEx.expect(UserEmailException.class);
            expectedEx.expectMessage("Domain name doesnt contain dot symbol");
            new User("John", "Wick", "johnwick@gmailcom", "strongpass");
        }
        @Test
        public void testCreateNewUserWithEmailWithSeveralDot() {
            expectedEx.expect(UserEmailException.class);
            expectedEx.expectMessage("Domain name contains several dot symbol in sequence");
            new User("John", "Wick", "johnwick@gmail..com", "strongpass");
        }
        @Test
        public void testCreateNewUserWithEmailWithAtAndDot() {
            expectedEx.expect(UserEmailException.class);
            expectedEx.expectMessage("Incorrect domain name in login");
            new User("John", "Wick", "johnwick@.gmailcom", "strongpass");
        }
        @Test
        public void testCreateNewUserWithEmailWithoutDomainTopLevel() {
            expectedEx.expect(UserEmailException.class);
            expectedEx.expectMessage("Empty top level domain in login");
            new User("John", "Wick", "johnwick@gmail.", "strongpass");
        }

        @Test
        public void testCreateNewUserWithNullPass() {
            expectedEx.expect(UserPassException.class);
            expectedEx.expectMessage("Password is null");
            new User("John", "Wick", "john_wick@gmail.com", null);
        }


        @Test
        public void testCreateNewUserWithShortPass() {
            expectedEx.expect(UserPassException.class);
            expectedEx.expectMessage("Password is too short");
            new User("John", "Wick", "john_wick@gmail.com", "stron");
        }

        @Test
        public void testCreateNewUserWithAvailableShortPass() {
            new User("John", "Wick", "john_wick@gmail.com", "strong");
        }

        @Test
        public void testCreateNewUserWithAvailableLongPass() {
            new User("John", "Wick", "john_wick@gmail.com", "strongstrong");
        }

        @Test
        public void testCreateNewUserWithLongPass() {
            expectedEx.expect(UserPassException.class);
            expectedEx.expectMessage("Password is too long");
            new User("John", "Wick", "john_wick@gmail.com", "123qwe123qwe1");
        }

        @Test
        public void testCreateNewUserWithLoginPass() {
            expectedEx.expect(UserPassException.class);
            expectedEx.expectMessage("Password contains login");
            new User("John", "Wick", "john_wick@gmail.com", "john_wick");
        }

        @Test
        public void testCreateNewUserWithFirstNamePass() {
            expectedEx.expect(UserPassException.class);
            expectedEx.expectMessage("Password contains first name");
            new User("John", "Wick", "john_wick@gmail.com", "John123");
        }
        @Test
        public void testCreateNewUserWithLastNamePass() {
            expectedEx.expect(UserPassException.class);
            expectedEx.expectMessage("Password contains last name");
            new User("John", "Wick", "john_wick@gmail.com", "Wick123");
        }
}
