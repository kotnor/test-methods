package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Александр on 09.10.2016.
 */
public class UserPassException extends IllegalArgumentException {
    public UserPassException(String message) {
        super(message);
    }
}
