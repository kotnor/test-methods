package ru.nsu.fit.endpoint.service.database.data;

import ru.nsu.fit.endpoint.service.database.exceptions.UserEmailException;
import ru.nsu.fit.endpoint.service.database.exceptions.UserFirstNameException;
import ru.nsu.fit.endpoint.service.database.exceptions.UserLastNameException;
import ru.nsu.fit.endpoint.service.database.exceptions.UserPassException;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class User {
    private UUID customerId;
    private UUID[] subscriptionIds;
    private UUID id;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String firstName;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String lastName;
    /* указывается в виде email, проверить email на корректность */
    private String login;
    /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
    private String pass;
    private UserRole userRole;

    public User(String firstName, String lastName, String login,
                String pass) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.pass = pass;
        this.userRole = UserRole.USER;

        validate(firstName, lastName, login, pass);
    }

    public static enum UserRole {
        COMPANY_ADMINISTRATOR("Company administrator"),
        TECHNICAL_ADMINISTRATOR("Technical administrator"),
        BILLING_ADMINISTRATOR("Billing administrator"),
        USER("User");

        private String roleName;

        UserRole(String roleName) {
            this.roleName = roleName;
        }

        public String getRoleName() {
            return roleName;
        }
    }

    public static void validate(String firstName, String lastName, String login, String pass) {
        validateFirstName(firstName);
        validateLastName(lastName);
        validateLogin(login);
        validatePassword(pass, login, firstName, lastName);
    }

    private static void validateFirstName(String firstName) {
        if (firstName == null)
            throw new UserFirstNameException("First name is null");
        if (firstName.contains(" "))
            throw new UserFirstNameException("First name contains space");
        if (firstName.length() < 2)
            throw new UserFirstNameException("First name is too short");
        if (firstName.length() > 12)
            throw new UserFirstNameException("First name is too long");
        if (!(Character.isLetter(firstName.charAt(0)) && Character.isUpperCase(firstName.charAt(0))))
            throw new UserFirstNameException("First name should start capital letter");
        String partName = firstName.substring(1);
        for (char x : partName.toCharArray()) {
            if (Character.isDigit(x)) {
                throw new UserFirstNameException("First name contains digit");
            }
            if (!Character.isLetter(x)) {
                throw new UserFirstNameException("First name contains wrong symbol");
            }
            if (Character.isUpperCase(x)) {
                throw new UserFirstNameException("There is a capital letter after 1 symbol");
            }

        }
    }
    private static void validateLastName(String lastName) {
        if (lastName == null)
            throw new UserLastNameException("Last name is null");
        if (lastName.contains(" "))
            throw new UserLastNameException("Last name contains space");
        if (lastName.length() < 2)
            throw new UserLastNameException("Last name is too short");
        if (lastName.length() > 12)
            throw new UserLastNameException("Last name is too long");
        if (!(Character.isLetter(lastName.charAt(0)) && Character.isUpperCase(lastName.charAt(0))))
            throw new UserLastNameException("Last name should start capital letter");
        String partName = lastName.substring(1);
        for (char x : partName.toCharArray()) {
            if (Character.isDigit(x)) {
                throw new UserLastNameException("Last name contains digit");
            }
            if (!Character.isLetter(x)) {
                throw new UserLastNameException("Last name contains wrong symbol");
            }
            if (Character.isUpperCase(x)) {
                throw new UserLastNameException("There is a capital letter after 1 symbol");
            }
        }
    }
    private static void validateLogin(String login) {
        if (!login.contains("@"))
            throw new UserEmailException("Login must be e-mail");
        if (login.substring(login.indexOf("@") + 1).contains("@"))
            throw new UserEmailException("E-mail must contain only one symbol @");
        if (login.startsWith("@"))
            throw new UserEmailException("Empty username in e-mail");
        if (!login.substring(login.indexOf("@") + 1).contains("."))
            throw new UserEmailException("Domain name doesnt contain dot symbol");
        if (login.substring(login.indexOf("@") + 1).contains(".."))
            throw new UserEmailException("Domain name contains several dot symbol in sequence");
        if (login.contains("@."))
            throw new UserEmailException("Incorrect domain name in login");
        if (login.charAt(login.length() - 1) == '.')
            throw new UserEmailException("Empty top level domain in login");
    }
    private static void validatePassword(String pass, String login, String firstName, String lastName) {
        if (pass == null) {
            throw new UserPassException("Password is null");
        }

        if (pass.length() < 6) {
            throw new UserPassException("Password is too short");
        }
        if (pass.length() > 12) {
            throw new UserPassException("Password is too long");
        }
        if (pass.contains(login.substring(0, login.indexOf("@")))) {
            throw new UserPassException("Password contains login");
        }
        if (pass.contains(firstName)) {
            throw new UserPassException("Password contains first name");
        }
        if (pass.contains(lastName)) {
            throw new UserPassException("Password contains last name");
        }
    }
}
