package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Александр on 09.10.2016.
 */
public class UserEmailException extends IllegalArgumentException {
    public UserEmailException(String message) {
        super(message);
    }
}
