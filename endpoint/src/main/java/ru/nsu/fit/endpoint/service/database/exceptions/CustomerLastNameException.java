package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Student_1 on 09.10.2016.
 */
public class CustomerLastNameException extends IllegalArgumentException {
    public CustomerLastNameException(String message) {
        super(message);
    }
}
