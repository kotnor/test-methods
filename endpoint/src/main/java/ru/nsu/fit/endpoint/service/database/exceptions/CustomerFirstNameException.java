package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Александр on 09.10.2016.
 */
public class CustomerFirstNameException extends IllegalArgumentException{
    public CustomerFirstNameException(String message) {
        super(message);
    }
}
