package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Александр on 09.10.2016.
 */
public class CustomerEmailException extends IllegalArgumentException {
    public CustomerEmailException(String message) {
        super(message);
    }
}
