package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Александр on 09.10.2016.
 */
public class PlanMinSeatsException extends IllegalArgumentException {
    public PlanMinSeatsException(String message) {
        super(message);
    }
}
