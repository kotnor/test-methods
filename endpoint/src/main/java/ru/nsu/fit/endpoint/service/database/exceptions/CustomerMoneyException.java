package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Александр on 09.10.2016.
 */
public class CustomerMoneyException extends IllegalArgumentException {
    public CustomerMoneyException(String message) {
        super(message);
    }
}
