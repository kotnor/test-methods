package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Александр on 09.10.2016.
 */
public class CustomerPassException extends IllegalArgumentException {
    public CustomerPassException(String message) {
        super(message);
    }
}
