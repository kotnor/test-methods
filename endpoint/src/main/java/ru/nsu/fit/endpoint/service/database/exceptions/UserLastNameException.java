package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Александр on 09.10.2016.
 */
public class UserLastNameException extends IllegalArgumentException {
    public UserLastNameException(String message) {
        super(message);
    }
}
