package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Александр on 09.10.2016.
 */
public class PlanDetailsException extends IllegalArgumentException {
    public PlanDetailsException(String message) {
        super(message);
    }
}
