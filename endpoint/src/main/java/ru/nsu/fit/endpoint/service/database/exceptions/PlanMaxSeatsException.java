package ru.nsu.fit.endpoint.service.database.exceptions;

import ru.nsu.fit.endpoint.service.database.data.Plan;

/**
 * Created by Александр on 09.10.2016.
 */
public class PlanMaxSeatsException extends IllegalArgumentException {
    public PlanMaxSeatsException(String message) {
        super(message);
    }
}
