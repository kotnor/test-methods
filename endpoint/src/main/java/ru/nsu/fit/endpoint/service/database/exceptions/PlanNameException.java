package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Александр on 09.10.2016.
 */
public class PlanNameException extends IllegalArgumentException {
    public PlanNameException(String message) {
        super(message);
    }
}
