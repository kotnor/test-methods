package ru.nsu.fit.endpoint.service.database.data;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.Validate;
import ru.nsu.fit.endpoint.service.database.exceptions.*;

import java.util.UUID;

import static java.lang.Character.*;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Customer {
    private UUID id;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String firstName;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String lastName;
    /* указывается в виде email, проверить email на корректность */
    private String login;
    /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
    private String pass;
    /* счет не может быть отрицательным */
    private int money;

    public Customer(String firstName, String lastName, String login, String pass, int money) {
        this.id = UUID.randomUUID();
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.pass = pass;
        this.money = money;
        validate(firstName, lastName, login, pass, money);
    }

    public static void validate(String firstName, String lastName, String login, String pass, int money) {
        if (firstName == null) {
            throw new CustomerFirstNameException("First name is null");
        }
        if (firstName.contains(" ")) {
            throw new CustomerFirstNameException("First name contains space");
        }
        if (firstName.length() < 2) {
            throw new CustomerFirstNameException("First name is too short");
        }
        if (firstName.length() > 12) {
            throw new CustomerFirstNameException("First name is too long");
        }
        if (isLowerCase(firstName.charAt(0))){
            throw new CustomerFirstNameException("First name should start capital letter");
        }
        for (int i = 0; i < firstName.length(); ++i) {
            char symbol = firstName.charAt(i);
            if (isDigit(symbol)) {
                throw new CustomerFirstNameException("First name contains digit");
            }
            if (!isLetterOrDigit(symbol)) {
                throw new CustomerFirstNameException("First name contains wrong symbol");
            }
            if (i != 0 && isUpperCase(symbol)) {
                throw new CustomerFirstNameException("There is a capital letter after 1 symbol");
            }
        }
        if (lastName == null) {
            throw new CustomerLastNameException("Last name is null");
        }
        if (lastName.length() < 2) {
            throw new CustomerLastNameException("Last name is too short");
        }
        if (lastName.length() > 12) {
            throw new CustomerLastNameException("Last name is too long");
        }
        if (lastName.contains(" ")) {
            throw new CustomerLastNameException("Last name contains space");
        }
        if (isLowerCase(lastName.charAt(0))){
            throw new CustomerLastNameException("Last name should start capital letter");
        }
        for (int i = 0; i < lastName.length(); ++i) {
            char symbol = lastName.charAt(i);
            if (isDigit(symbol)) {
                throw new CustomerLastNameException("Last name contains digit");
            }
            if (!isLetterOrDigit(symbol)) {
                throw new CustomerLastNameException("Last name contains wrong symbol");
            }
            if (i != 0 && isUpperCase(symbol)) {
                throw new CustomerLastNameException("There is a capital letter after 1 symbol");
            }
        }
        if (!login.contains("@")) {
            throw new CustomerEmailException("Login must be e-mail");
        }
        if (login.substring(login.indexOf('@') + 1).contains("@") ) {
            throw new CustomerEmailException("E-mail must contain only one symbol @");
        }
        if (login.startsWith("@")) {
            throw new CustomerEmailException("Empty username in e-mail");
        }
        if (!login.substring(login.indexOf("@")).contains(".")) {
            throw new CustomerEmailException("Domain name doesnt contain dot symbol");
        }
        if (login.substring(login.indexOf("@")).contains("..")) {
            throw new CustomerEmailException("Domain name contains several dot symbol in sequence");
        }
        if (login.contains("@.")) {
            throw new CustomerEmailException("Incorrect domain name in login");
        }
        if (login.charAt(login.length() - 1) == '.') {
            throw new CustomerEmailException("Empty top level domain in login");
        }
        if (pass == null) {
            throw new CustomerPassException("Password is null");
        }

        if (pass.length() < 6) {
            throw new CustomerPassException("Password is too short");
        }
        if (pass.length() > 12) {
            throw new CustomerPassException("Password is too long");
        }
        if (pass.contains(login.substring(0, login.indexOf("@")))) {
            throw new CustomerPassException("Password contains login");
        }
        if (pass.contains(firstName)) {
            throw new CustomerPassException("Password contains first name");
        }
        if (pass.contains(lastName)) {
            throw new CustomerPassException("Password contains last name");
        }
        if (money < 0) {
            throw new CustomerMoneyException("Account must be positive value");
        }
    }

    public UUID getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLogin() {
        return login;
    }

    public String getPass() {
        return pass;
    }

    public int getMoney() {
        return money;
    }
}
