package ru.nsu.fit.endpoint.service.database.data;

import ru.nsu.fit.endpoint.service.database.exceptions.*;

import java.util.UUID;

import static java.lang.Character.isLetterOrDigit;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Plan {
    private UUID id;
    /* Длина не больше 128 символов и не меньше 2 включительно не содержит спец символов */
    private String name;
    /* Длина не больше 1024 символов и не меньше 1 включительно */
    private String details;
    /* Не больше 999999 и не меньше 1 включительно */
    private int maxSeats;
    /* Не больше 999999 и не меньше 1 включительно, minSeats >= maxSeats */
    private int minSeats;
    /* Больше ли равно 0 но меньше либо равно 999999 */
    private int feePerUnit;

    public Plan(String name, String details, int maxSeats, int minSeats, int feePerUnit) {
        validate(name, details, maxSeats, minSeats, feePerUnit);
        this.id = UUID.randomUUID();
        this.name = name;
        this.details = details;
        this.maxSeats = maxSeats;
        this.minSeats = minSeats;
        this.feePerUnit = feePerUnit;
    }

    public static void validate(String name, String details, int maxSeats, int minSeats, int feePerUnit) {
        if (name == null) {
            throw new PlanNameException("Service Plan Name is null");
        }
        if (name.length() < 2) {
            throw new PlanNameException("Service Plan Name is too short");
        }
        if (name.length() > 128) {
            throw new PlanNameException("Service Plan Name is too long");
        }
        for(char symbol: name.toCharArray()) {
            if (!isLetterOrDigit(symbol)) {
                throw new PlanNameException("Service Plan Name contains wrong symbol");
            }
        }
        if (details == null) {
            throw new PlanDetailsException("Service Plan details is null");
        }
        if (details.length() < 1) {
            throw new PlanDetailsException("Service Plan details length is too short");
        }
        if (details.length() > 1024) {
            throw new PlanDetailsException("Service Plan details length is too long");
        }
        if (maxSeats < 1) {
            throw new PlanMaxSeatsException("Service Plan max seats value is too small");
        }
        if (maxSeats > 999999) {
            throw new PlanMaxSeatsException("Service Plan max seats value is too big");
        }
        if (minSeats < 1) {
            throw new PlanMinSeatsException("Service Plan min seats value is too small");
        }
        if (minSeats > 999999) {
            throw new PlanMinSeatsException("Service Plan min seats value is too big");
        }
        if (minSeats > maxSeats) {
            throw new PlanMinSeatsException("Service Plan min seats value bigger short than max seats");
        }
        if (feePerUnit < 0) {
            throw new PlanFeePerUnitException("Service Plan feePerUnit value is too small");
        }
        if (feePerUnit > 999999) {
            throw new PlanFeePerUnitException("Service Plan feePerUnit value is too big");
        }
    }
}