package ru.nsu.fit.endpoint.service.database.exceptions;

import java.util.IllegalFormatCodePointException;

/**
 * Created by Александр on 09.10.2016.
 */
public class PlanFeePerUnitException extends IllegalArgumentException {
    public PlanFeePerUnitException(String message) {
        super(message);
    }
}
