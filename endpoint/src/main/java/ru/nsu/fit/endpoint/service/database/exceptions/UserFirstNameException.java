package ru.nsu.fit.endpoint.service.database.exceptions;

/**
 * Created by Александр on 09.10.2016.
 */
public class UserFirstNameException extends IllegalArgumentException {
    public UserFirstNameException(String message) {
        super(message);
    }
}
